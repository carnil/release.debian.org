Hi there!

As promised, here comes an update from the release team. After we spoke
with several teams recently, we have tried to come up with a plan for
the upcoming transitions, freeze and release of Debian Squeeze.

The situation of the release is not as good as we have hoped, but it
looks like we can do the release in a few months if we all work
together. Below is a list of bigger transitions and issues we are
currently aware of. If you think we've missed something, send a mail to
debian-release@lists.debian.org as soon as possible. 

Current Transitions
===================
The good news is that the imagemagick and liblo transitions have just been
completed. We are also working hard on transitioning to a new parted
version, bringing many new features to Squeeze. Together with the
recent efforts for an X.org-based graphical installer, this will allow a
release of a debian-installer version quite near to what we will use for
Squeeze in the near future.

We are also working towards the last really enormous transition for the
Squeeze cycle: Making Python 2.6 the default python version. In the past
few months, packages supporting multiple python versions have been
rebuilt to also support python2.6. The remaining work is to transition
all packages that can only support one python version at a time. There
are still some bugs needing to be resolved for that to happen [1], but
we consider it doable in the near future.

 [1] http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=python2.6;users=debian-python@lists.debian.org

There are some other transitions that we are aware of that are currently
prepared and/or already going on:
  * eglibc 2.11
     This is currently pending because of an problem on hppa. There also
     have been some unexpected test failures on mips*, but work on this
     is progressing.
  * ruby1.9.1
     ruby1.9 will be replaced by ruby1.9.1. This is half-done at this
     point.
  * BLAS/LAPACK
     A new system will be set in place to allow switching between
     different, optimized versions of these linear algebra libraries.
  * Tcl/Tk 8.4/8.5
     Tcl 8.3 will be replaced by newer versions. This transition is
     currently staged in experimental.
  * mpi-defaults
     Transitioning away from MPICH and LAM/MPI is currently prepared.
     They will be replaced by MPICH2 and OpenMPI.
  * linux-2.6/linux-base libata transition
     New drivers using 'libata' are about to replace the old IDE
     drivers. linux-base will prompt you on x86 to upgrade your configuration
     files (fstab, bootloader) to refer to stable UUIDs instead of
     unpredictable device names.

Some other big transitions have been planned, but not yet started:
  * directfb
     A SONAME bump for directfb is planned and this transition should
     happen soonish.
  * Qt4.6 and KDE 4.4
     The new upstream versions of these packages have been prepared by
     their maintainers, but due to the complex situation in unstable
     weren't uploaded yet. Nonetheless, they are planned to be included
     in Squeeze.
  * GNOME 2.30 and Evolution
     Likewise, we are expecting to ship with Gnome 2.30. This will bring
     some smaller transitions.

We are also aware of planned updates to icu, boost1.42 and icedove3.


Timeline
========
We expect to need two to four weeks for the Python transition, blocking
most of the testing transition. We are currently expecting to be done
with the python switch at the end of april. Afterwards, we
will need to get everything done that couldn't move due to python2.6.

A freeze in late may/june seems to be possible, if people work hard on
getting these transitions done.


Release Goals
=============
* Full IPv6 support
  Sadly it looks like that goal won't be achieved in time; there are
  currently over 100 bugs open, and many applications still miss proper
  IPv6 support.  But it looks far better than with previous releases.
  
  See http://wiki.debian.org/ReleaseGoals/FullIPv6Support
  and http://bugs.debian.org/cgi-bin/pkgreport.cgi?which=tag&data=ipv6&archive=no
  for details.

* Large File Support 
  Is almost done.  We currently know of only nine bugs (some of them
  minor) affecting this release goals.
  
  See http://wiki.debian.org/ReleaseGoals/LFS
  and http://bugs.debian.org/cgi-bin/pkgreport.cgi?which=tag&data=lfs&archive=no
  for details.

* New source package format support
  After our infrastructure is now capable of handling the new
  dpkg-source format, having all packages be compatible with the new
  dpkg-source as default source format is about 80 bugs away.
  Some of these bugs are very easy to fix by adding a proper
  debian/source/format.

  See http://wiki.debian.org/ReleaseGoals/NewDebFormats
  and http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=hertzog@debian.org;tag=3.0-quilt-by-default
  for details.

* Removal of OSS
  Only three bugs remain to be resolved to get this release goal
  achieved.

  See http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org;tag=oss-removal
  for details. 

* Removal of unneeded .la files
  This release goal seems not be achieved with squeeze 
  
  See http://wiki.debian.org/ReleaseGoals/LAFileRemoval for details.

* MultiArch
  dpkg lacks support for it, so it sadly looks like this release goal
  won't be achieved either.

  See http://wiki.debian.org/ReleaseGoals/MultiArch for details.

* GNU/kFreeBSD-*
  The release of these two new architectures looks promising, but they
  are still far away from full archive coverage.  It seems that much
  could be gained by fixing some key packages.

  Please contact the bsd porter list if you would like to join their
  effort: http://lists.debian.org/debian-bsd/.

* Removing obsolete GNOME libraries:
  While there has been quite an improvement compared to previous
  releases, we are still ~40 bugs away to achieve this release goal.

  Please see http://wiki.debian.org/ReleaseGoals/RemoveOldLibs and 
  http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=pkg-gnome-maintainers@lists.alioth.debian.org;tag=oldlibs
  for details.

== How to Help ==
Since there is an enormous amount of RC bugs, you can help facilitate
the release by fixing RC bugs in your own packages. Other maintainers
might not be able to do this for their packages, so you might check if
things you use are not in a release stable and help out, if needed.

We would be happy if you would only upload software that you believe to
be stable at this point. Packaging a new, unstable version might lead to
unexpected problems and could delay the release, so consider using
experimental for any new hot features you want to bring into Debian.

Further help is needed in triaging RC bugs and bugs concerning our
release goals. To that end, it might be enough to give the relevant
people a nudge include existing fixes.

You might also be interested in working on the release notes:
http://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=release-notes;dist=unstable

== Administriva ==
At present there is no official Release Manager, and all release
team members are considered to have full authority on all matters.

We are also looking for new members of the release team. If you don't
know what needs to be done, here's a short overview:
 * Transitions waiting to happen could profit from someone checking if
   everything is alright. This means finding out which packages are
   affected, which of these need sourceful uploads and coordination with
   the maintainers of these packages.
   If the transition is meant to go smoothly into testing, care needs to
   be taken to not entangle it with any of the other ongoing transitions.
   When all of that is checked and prepared, the transitioning package
   should be uploaded and binNMUs be scheduled.

 * We also need to get the number of rc bugs down if we want to release
   squeeze this year. To this end, it would be great if one or more
   BSPs could be organized, possibly focussed at a specific type
   of bug. 
   Besides the number of RC bugs, we also have some widely used packages
   where maintainers are overwhelmed by the number of bugs filed against
   their packages (KDE, Gnome, iceweasel, ...) While not strictly a release
   issue, triaging (and possibly fixing) bugs can be integrated into a BSP
   to give people something easier (but nonetheless tedious :-/) to do.

Greetings,
NN
