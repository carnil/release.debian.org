To: debian-devel-announce@lists.debian.org
Subject: Transition status; please no new shlib bumps

Hi all,

we currently have a couple (or rather: way too many) transitions already
ongoing.  Please, don't upload shlib bumps or lib renamings unless
required by one of these transitions.


glibc 2.3.5
===========
This is the transition that blocks almost all packages from reaching
testing.  Currently, we have with 2.3.5-5 the fourth version of glibc in
unstable, and we hope that this one can go to testing - but we'll see in
the next days.


C++-ABI-transition
==================
The second-largest transition is the C++-ABI-transition. Please see
http://lists.debian.org/debian-devel-announce/2005/07/msg00001.html for
details about the transition.

Also, KDE is going from 3.3 to 3.4 in parallel with its C++-abi-transition.


X.org
=====
A pre-requisite for a bunch of packages to enter testing is that the X.org
packages enter testing first.  X.org depends (besides solving two RC-bugs
which basically just require new binary dependencies) not only on a newer
glibc, but also on a more current libselinux-version entering testing.  This
should happen fast after glibc is in.


Gnome 2.10
==========
We need to bring Gnome 2.10 to testing before 2.12 hits unstable. There is
not very much that needs to be done for that, but of course, glibc and
X.org needs to be completed first.  As the situation is, we cannot really
estimate how much effort Gnome requires, until we can try to really bring
it in.


Further transitions
===================
Please don't do.

Any further transition has the potential to make it even harder to bring
packages to testing, as this makes the barrier for packages to go to
testing even higher. The reason is that it resets the counter for
testing migration (and as large transitions have quite many packages
involved, a random reset every now is easily fatal for the transition).
If we notice another transition, shlibbump or (library) package renaming
that endangers any of the transitions above, we might be forced to
remove some or even all packages depending on the violating package; if
you manage that it goes in parallel with some other transition (like the
kde 3.3 to 3.4 one with the C++-abi-one), this would work.  But there is
always a danger, so please coordinate in time, and accept that a "please
don't" is sometimes the best solution and not meant personally.  As the
current status is that we have too many transitions, we need to resolve
that in the fastest possible way.



Cheers,
Andi
