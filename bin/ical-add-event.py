#!/usr/bin/python3 -u

import argparse
import datetime
import os
import re
import sys
import uuid

import icalendar
import pytz


args = None


def parse_date(input):
    input = re.sub(r'^\s+', '', input)
    input = re.sub(r'\s+$', '', input)

    match = re.search(
                r'^(\d{4})[/-]?(\d{2})[/-]?(\d{2})$',
                input,
            )
    if match:
        return datetime.date(
                    year=int(match.group(1)),
                    month=int(match.group(2)),
                    day=int(match.group(3)),
               )

    match = re.search(
                (
                  r'^'
                  r'(\d{4})[/-]?(\d{2})[/-]?(\d{2})'
                  r'[T ]?'
                  r'(\d{2})[:-]?(\d{2})[:-]?(\d{2})'
                  r'$'
                ),
                input,
            )
    if match:
        return datetime.datetime(
                    year=int(match.group(1)),
                    month=int(match.group(2)),
                    day=int(match.group(3)),
                    hour=int(match.group(4)),
                    minute=int(match.group(5)),
                    second=int(match.group(6)),
               ).replace(tzinfo=pytz.utc)

    raise Exception('Failed to parse date: %s' % input)


def ical_add_event():
    event = icalendar.Event()

    # BEGIN:VEVENT
    # CREATED:20171125T133138Z
    # LAST-MODIFIED:20171125T133550Z
    # DTSTAMP:20171125T133550Z
    # UID:923d8360-4645-4b94-a078-66388bfad7fc
    # SUMMARY:Stretch point release 9.3 and Jessie point release 8.10
    # DTSTART;VALUE=DATE:20171209
    # DTEND;VALUE=DATE:20171210
    # TRANSP:TRANSPARENT
    # DESCRIPTION:[Stretch]: https://lists.debian.org/debian-release/2017/11/msg   # noqa: E501
    #  00273.html\n[Jessie]: https://lists.debian.org/debian-release/2017/11/msg0  # noqa: E501
    #  0274.html\n
    # SEQUENCE:1
    # X-MOZ-GENERATION:1
    # END:VEVENT

    now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)

    properties = {
        'created':          icalendar.vDatetime(now).to_ical(),
        'last-modified':    icalendar.vDatetime(now).to_ical(),
        'dtstamp':          icalendar.vDatetime(now).to_ical(),
        'uid':              uuid.uuid4(),
        'summary':          args.summary,
        'transp':           'TRANSPARENT',
        'description':      args.description,
        'sequence':         1,
        'x-moz-generation': 1,
    }

    for key in properties:
        if args.debug:
            print("%s = %s" % (key, properties[key]))

        event[key] = properties[key]

    for key in [
                 'dtstart',
                 'dtend',
               ]:
        if args.debug:
            print("%s = %s" % (key, args.__dict__[key]))

        event.add(key, args.__dict__[key])

    if args.debug:
        print("")

    if args.verbose:
        print("Reading file: %s" % args.file)

    data = b''

    with open(args.file, 'rb') as f:
        for line in f.readlines():
            if line.startswith(b'END:VCALENDAR'):
                if args.verbose:
                    print("Adding event:\n\n%s\n" % event.to_ical().decode())

                data += event.to_ical()

            data += line

    if args.debug:
        print(data.decode())

    if args.verbose:
        print("Writing file: %s" % args.file)

    with open(args.file, 'wb') as f:
        f.write(data)


def main():
    global args

    default = {
                'file': os.path.abspath(
                          os.path.join(
                            os.path.dirname(
                              os.path.dirname(__file__),
                            ),
                            'www',
                            'release-calendar.ics',
                          )
                        ),
              }

    parser = argparse.ArgumentParser(
                 formatter_class=argparse.RawDescriptionHelpFormatter,
             )

    parser.add_argument(
                         '-s', '--summary',
                         metavar='<STRING>',
                         action='store',
                         help='Text for event summary',
                       )
    parser.add_argument(
                         '-d', '--description',
                         metavar='<STRING>',
                         action='store',
                         help='Text for event description',
                       )
    parser.add_argument(
                         '-D', '--date',
                         metavar='<DATE>',
                         action='store',
                         help='Date for all-day event',
                       )
    parser.add_argument(
                         '-S', '--dtstart',
                         metavar='<DATE>[<TIME>]',
                         action='store',
                         help='Date for event start',
                       )
    parser.add_argument(
                         '-E', '--dtend',
                         metavar='<DATE>[<TIME>]',
                         action='store',
                         help='Date for event end',
                       )
    parser.add_argument(
                         '-f', '--file',
                         metavar='<PATH>',
                         action='store',
                         help='Path to iCal file (default: %s)' % (
                           default['file'],
                         ),
                         default=default['file'],
                       )
    parser.add_argument(
                         '-g', '--debug',
                         action='store_true',
                         help='Enable debug output',
                       )
    parser.add_argument(
                         '-v', '--verbose',
                         action='store_true',
                         help='Enable verbose output',
                       )

    args = parser.parse_args()

    if not args.summary:
        args.summary = input("Summary: ")

    if not args.description:
        args.description = input("Description: ")

    if args.date:
        args.dtstart = parse_date(args.date)

        args.dtend = args.dtstart + datetime.timedelta(days=1)

    if not args.dtstart:
        args.dtstart = parse_date(input("Start date: "))
    elif type(args.dtstart) is str:
        args.dtstart = parse_date(args.dtstart)

    if not args.dtend:
        args.dtend = parse_date(input("End date: "))
    elif type(args.dtend) is str:
        args.dtend = parse_date(args.dtend)

    ical_add_event()

    sys.exit(0)


if __name__ == "__main__":
    main()
