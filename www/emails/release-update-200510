To: debian-devel-announce@lists.debian.org
Subject: Transition time: KDE, JACK, arts, sablotron, unixodbc, net-snmp, php, ...

It's been a long trip, and as usual not everything has gone according to
plan; but we are now nearing the point of being able to get the bulk of
the C++ ABI transition pushed into testing.  At this point, most of the
packages waiting to be updated in testing are tied to this transition in
one way or another; including all of these libraries that are undergoing
ABI transitions:

  arts
  coin2
  cppunit
  dbus
  dynamite
  geos
  flac
  freetds
  gdal
  heartbeat
  icu
  imagemagick
  jack-audio-connection-kit
  kdelibs
  kdemultimedia
  kdepim
  libfwbuilder
  libkexif
  libkipi
  libmodplug
  libmusicbrainz-2.1
  libpqxx
  libsidplay
  libtunepimp
  libxbase
  openexr
  orange
  net-snmp
  qscintilla
  qt-x11-free
  sablotron
  sidplay-libs
  taglib
  unixodbc
  vdk2
  vdkxdb2
  vtk
  wv2
  xbsql
  xerces25
  xerces26

As a result, any packages that have versions in testing and depend on
one of these libraries must be updated at the same time.  For the first
time over the past months, we are now able to get a comprehensive look
at just which packages are involved in this transition -- around 300
source packages that need to be updated!  As a result, the release
team asks that the maintainers refrain from uploads of these packages for
any reason without coordination with the release team, until this
transition completes; uncoordinated uploads will most likely lead to your
package being removed from testing to let the transition complete.  For your
reference, a list of source packages known to be involved, grouped by
maintainer, is attached.

In addition, this transition is in danger of being tied to a very
untimely shlibs bump in libstdc++6, and the release team is working out
whether certain affected packages should be removed temporarily from
testing in order to avoid further delays.  The list of those packages is
included as the second attachment to this mail.

If all goes well, we are very close to being able to get this monster
transition through, putting us over the hump on the C++ ABI change for
etch.  Please continue coordinating with the release team before
starting any new library transitions, though, so that we don't find
testing wedged behind *another* such clump of libraries. :)

One other library transition that bears commenting at this point is the
libssl0.9.7->libssl0.9.8 transition, about which there has already been
a fair amount of discussion on debian-devel.  This transition was not
anticipated by the release team, but libssl0.9.8 has reached testing so
there is no reason to worry about your package being rebuilt against
libssl0.9.8 instead of libssl.0.9.7.  However, this does *not* mean that
you should do a sourceful upload of your package just for the libssl
transition -- least of all if your package is already tied up in another
lib transition.  First, we are finding some problems with packages built
against the new libssl which appear to be bugs in the openssl package
itself, such that if you don't have another reason to upload, your users
are probably better served with libssl0.9.7 for the moment.  Second,
thanks to some enhancements Ryan Murray has recently made to
buildd/wanna-build, it is now possible for the release team to request
automated buildd binNMUs of a package across all architectures for
library transitions, sparing maintainers the trouble of doing
rebuild-only sourceful uploads.  As some of you have already discovered,
this is turning up bugs in packages that can't be binNMUed because of
arch: any packages which depend on (= ${Source-Version}) of an arch: all
package.  Please help make future library transitions easier by making
sure your packages are binNMU-safe.

That's all the news for now; look forward to hearing more about the end
of the KDE transition and the first d-i beta for etch in the near
future!

--
Steve Langasek                                     [vorlon@debian.org]
Debian Release Team

Packages that must be updated together as part of the C++ ABI 
transition, grouped by maintainer

Guenter Geiger (Debian/GNU) <geiger@debian.org>
   jack-rack
   ladcca
   libjackasyn
   meterbridge
   puredata
   qjackctl
   snd
   stk

Laszlo Boszormenyi (GCS) <gcs@debian.hu>
   libsidplay
   sidplay
   sidplay-base
   sidplay-libs
   tuxeyes
   xsidplay

Michael Ablassmeier <abi@grinser.de>
   kstreamripper

Ryuichi Arafune <arafune@debian.org>
   imagemagick

Christian Bayle <bayle@debian.org>
   php4-mcrypt

Romain Beauxis <toots@rastageeks.org>
   kshutdown

Bradley Bell <btb@debian.org>
   kaptain

Jay Berkenbilt <qjb@debian.org>
   icu
   vips

Eduard Bloch <blade@debian.org>
   cdcat

A. Maitland Bottoms <bottoms@debian.org>
   icomlib
   vtk

Regis Boudin <regis@boudin.name>
   tellico

Jeremy T. Bouse <jbouse@debian.org>
   fwbuilder
   libfwbuilder

Chris Boyle <cmb@debian.org>
   klogic

Goswin von Brederlow <brederlo@informatik.uni-tuebingen.de>
   ifstat

Paul Brossier <piem@debian.org>
   noteedit
   xmms-jackasyn

Eric Van Buggenhaut <ericvb@debian.org>
   fluidsynth
   qsynth

Daniel Burrows <dburrows@debian.org>
   tse3

Ben Burton <bab@debian.org>
   kdeedu
   kdesdk
   kile

Ross Burton <ross@debian.org>
   sound-juicer

Giacomo Catenazzi <cate@debian.org>
   knapster2

Hubert Chan <hubert@uhoreg.ca>
   alsaplayer

Christopher L Cheney <ccheney@debian.org>
   taglib
   vorbis-tools

Pierre Chifflier <chifflier@cpe.fr>
   esvn

Volker Christian <voc@users.sourceforge.net>
   synce-kde

Volker Christian <voc@debian.org>
   dynamite
   kcemirror
   orange
   unshield

Paul Cupis <paul@cupis.co.uk>
   guarddog
   guidedog

LI Daobing <lidaobing@gmail.com>
   qterm

Debian Hamradio Maintainers <debian-hams@lists.debian.org>
   linpsk

Debian Boost Team <pkg-boost-devel@lists.alioth.debian.org>
   boost

Debian OpenOffice Team <debian-openoffice@lists.debian.org>
   oooqs

Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
   arts
   kdeaccessibility
   kdeadmin
   kdeartwork
   kdebase
   kdebindings
   kdegames
   kdegraphics
   kdelibs
   kdemultimedia
   kdenetwork
   kdepim
   kdetoys
   kdeutils
   koffice
   qt-x11-free
   wv2

Benjamin Drieu <benj@debian.org>
   prestimel

Peter Eisentraut <petere@debian.org>
   kdissert
   kmldonkey
   licq
   pinentry
   psqlodbc
   rekall

Free Ekanayaka <free@agnula.org>
   clalsadrv
   creox
   jackeq

Jarno Elonen <elonen@debian.org>
   agistudio

Rene Engelhard <rene@debian.org>
   kover

Helen Faulkner <helen@debian.org>
   kaquarium
   kcpuload
   kdoomsday
   kfish
   knetload
   labplot

Bartosz Fenski <fenio@debian.org>
   imgseek
   moaggedit

Javier Fernandez-Sanguino Pen~a <jfs@computer.org>
   cheops

Turbo Fredriksson <turbo@debian.org>
   php4-idn

Jochen Friedrich <jochen@scram.de>
   mp3blaster
   net-snmp
   snmptrapfmt

Gerfried Fuchs <alfie@debian.org>
   xmms-sid

Mike Furr <mfurr@debian.org>
   terminatorx
   xmms-jack

Igor Genibel <igenibel@debian.org>
   kexi

John Goerzen <jgoerzen@complete.org>
   zsafe

Miah Gregory <mace@debian.org>
   qbble

Debian QA Group <packages@qa.debian.org>
   bbconf
   icemc
   ksocrat
   mysql-navigator
   okle
   php4-interbase

Debian XML/SGML Group <debian-xml-sgml-pkgs@lists.alioth.debian.org>
   libxml-xerces-perl
   xerces25
   xerces26

Francois Gurin <matrix@debian.org>
   kismet

Steve Halasz <debian@adkgis.org>
   geos
   qgis

Christian Hammers <ch@debian.org>
   madman

Andres Seco Hernandez <AndresSH@debian.org>
   swscanner

Henrique de Moraes Holschuh <hmh@debian.org>
   timidity

Simon Horman <horms@debian.org>
   heartbeat

Morten Hustveit <morten@debian.org>
   kwavecontrol

Mark Hymers <mark.hymers@ncl.ac.uk>
   ifrit

Masami Ichikawa <hangar-18@mub.biglobe.ne.jp>
   bookmarkbridge

Alberto Gonzalez Iniesta <agi@inittab.org>
   hotswap
   kmyfirewall

Aurelien Jarno <aurel32@debian.org>
   camstream
   klineakconfig
   ksensors
   ksimus
   ksimus-boolean
   ksimus-datarecorder
   ksimus-floatingpoint
   lineak-kdeplugins
   quiteinsane
   quiteinsanegimpplugin
   rt2400
   rt2500

Robert Jordens <jordens@debian.org>
   bitscope
   gpib
   jack-audio-connection-kit
   jack-tools
   jamin
   libtunepimp
   syck
   timemachine

Theodore Karkoulis <bilbo@debian.org>
   kbarcode

Peter Karlsson <peterk@debian.org>
   turqstat

Jean-Michel Kelbert <kelbert@debian.org>
   karamba
   kbiff
   komba2

Matthias Klose <doko@debian.org>
   doxygen
   sqlrelay

Gerd Knorr <kraxel@debian.org>
   krecord

Daniel Kobras <kobras@debian.org>
   dx

Jano Kupec <jkupec@zoznam.sk>
   albumshaper

Joshua Kwan <joshk@triplehelix.org>
   flac
   nethack

Noèl Köthe <noel@debian.org>
   kde-i18n
   mbrowse
   openipmi

Jeremy Lainé <jeremy.laine@m4x.org>
   kprof
   sailcut

Wesley J. Landaker <wjl@icecavern.net>
   drawtiming

Steve Langasek <vorlon@debian.org>
   freetds
   libdbd-sybase-perl
   myodbc
   php-imlib
   sqsh
   unixodbc

Siggi Langauf <siggi@debian.org>
   xine-lib

Andrew Lau <netsnipe@users.sourceforge.net>
   openexr

Berin Lautenbach <berin@debian.org>
   xalan

Simon Law <sfllaw@debian.org>
   wvstreams

Roger Leigh <rleigh@debian.org>
   libpqxx

Marc Leeman <marc.leeman@gmail.com>
   dvdauthor

Rafal Lewczuk <rlewczuk@pronet.pl>
   xbsql

Bernhard R. Link <brlink@debian.org>
   cuyo

Ana Beatriz Guerrero Lopez <anja_isbilia@yahoo.es>
   kdbg

Francesco Paolo Lovergine <frankie@debian.org>
   smb4k

Eduardo Marcel Macan <macan@debian.org>
   specimen

Marcelo E. Magallon <mmagallo@debian.org>
   view3ds

Debian ALSA Maintainers <pkg-alsa-devel@lists.alioth.debian.org>
   alsa-plugins

Debian PHP Maintainers <pkg-php-maint@lists.alioth.debian.org>
   php-db
   php-http
   php-imap
   php-mail
   php-net-smtp
   php-net-socket
   php-xml-parser
   php4
   php5

Pedro Jurado Maqueda <melenas@kdehispano.org>
   kiosktool

Ivo Marino <eim@mentors.debian.net>
   libaudio-flac-header-perl

Christopher Martin <chrsmrtn@debian.org>
   gwenview

Peter Mathiasson <peterm@debian.org>
   gpgkeys

Luis Mayoral <mayoral@linuxadicto.org>
   kompose

Alastair McKinstry <mckinstry@debian.org>
   kvdr

Jose Carlos Medeiros <debian@psabs.com.br>
   php4-imagick

Ricardo Javier Cardenes Medina <rcardenes@debian.org>
   python-qt3
   qscintilla
   sip-qt3
   sip4-qt3

Michael Meskes <meskes@debian.org>
   tora

Jonas Meurer <mejo@debian.org>
   lurker

Samuel Mimram <smimram@debian.org>
   linphone

Steffen Moeller <moeller@pzr.uni-rostock.de>
   qtdmm

Hamish Moffatt <hamish@debian.org>
   phaseshift

Juan Manuel Garcia Molina <juanma@debian.org>
   facturalux
   gkrellm-snmp
   ksociograma

Gustavo R. Montesino <grmontesino@ig.com.br>
   libgda2

David L. Moreno <david.lopez.moreno@hispalinux.es>
   kimdaba

Tommaso Moroni <moronito@debian.org>
   knights

Oleksandr Moskalenko <malex@tagancha.org>
   scribus

Sebastian Muszynski <do2ksm@linkt.de>
   kpsk

Jan Niehusmann <jan@debian.org>
   psi
   qca
   qca-tls

Matthew Palmer <mpalmer@debian.org>
   php4-sqlite

Alejandro Exojo Piqueras <suy@badopi.org>
   konserve

Zed Pobre <zed@debian.org>
   libmodplug
   modplugxmms
   xexec

Ari Pollak <ari@debian.org>
   libsdl-sound1.2

Tomas Pospisek <tpo_deb@sourcepole.ch>
   xxdiff

Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
   grass

Mark Purcell <msp@debian.org>
   hpoj
   kfocus

Arnaud Quette <aquette@debian.org>
   knutclient
   nut

Filip Van Raemdonck <mechanix@debian.org>
   mdbtools

Angel Ramos <seamus@debian.org>
   knetfilter
   krusader

Silke Reimer <silke.reimer@intevation.de>
   gdal

Elimar Riesebieter <riesebie@lxtec.de>
   moc

Steve M. Robbins <smr@debian.org>
   coin2
   kseg
   pdftoipe
   soqt

Jaime Robles <jaime@debian.org>
   klog

José L. Redrejo Rodríguez <jredrejo@edu.juntaextremadura.net>
   gambas

Andreas Rottmann <rotty@debian.org>
   libmusicbrainz-2.1

Ludovic Rousseau <rousseau@debian.org>
   xcardii

Miriam Ruiz <little_miry@yahoo.es>
   avida

Andres Salomon <dilinger@debian.org>
   libmusicbrainz-ruby

Otavio Salvador <otavio@debian.org>
   graveman

Bruno Sant'Anna <brunocesar@ajato.com.br>
   apollon

Eike Sauer <eikes@cs.tu-berlin.de>
   kdiff3

Pasi Savilaakso <pasi.savilaakso@pp.inet.fi>
   valknut

Christoffer Sawicki <qerub@home.se>
   gtk-qt-engine

Mike Schacht <mschacht@alumni.washington.edu>
   kdirstat

Daniel Schepler <schepler@debian.org>
   qtads

David Schleef <ds@schleef.org>
   ktimetrace

Gürkan Sengün <gurkan@linuks.mine.nu>
   cynthiune.app

Guus Sliepen <guus@debian.org>
   wireless-tools

Radu Spineanu <radu@timisoara.roedu.net>
   fet

Joop Stakenborg <pa3aba@debian.org>
   hamfax
   marote
   qsstv
   unixcw

Uwe Steinmann <steinm@debian.org>
   netmrg
   php4-ps

Al Stone <ahs3@debian.org>
   oprofile

James Stone <jmstone@dsl.pipex.com>
   rlplot

Ondřej Surý <ondrej@debian.org>
   sablotron

Jose Luis Tallon <jltallon@adv-solutions.net>
   kcheckgmail
   picwiz

Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
   libkexif
   libkipi

D-Bus Maintainance Team <debian-dbus@fooishbar.org>
   dbus

Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
   kphone

Frank S. Thomas <frank@thomas-alfeld.de>
   kboincspy

Tobias Toedter <t.toedter@gmx.net>
   qbrew

James Troup <james@nocrew.org>
   filelight

Nathaniel W. Turner <nate@houseofnate.net>
   konversation

Junichi Uekawa <dancer@debian.org>
   ecasound2.2

Thibaut VARENE <varenet@debian.org>
   libapache-mod-musicindex

Robin Verduijn <robin@debian.org>
   kvirc

Michael Vogt <mvo@debian.org>
   libxbase
   vdk2
   vdkxdb2

Riku Voipio <riku.voipio@iki.fi>
   gmod

Christoph Wegscheider <christoph.wegscheider@wegi.net>
   potracegui

David N. Welton <davidw@debian.org>
   tclmagick

Torsten Werner <twerner@debian.org>
   animal

Eric Wong <eric@petta-tech.com>
   mpd

Joe Wreschnig <piman@debian.org>
   pyflac
   pymodplug

Anton Zinoviev <zinoviev@debian.org>
   kbedic

Marco van Zwetselaar <zwets@zwets.com>
   qtstalker

sean finney <seanius@debian.org>
   cacti-cactid

Maintainers of GStreamer packages 
<pkg-gstreamer-maintainers@lists.alioth.debian.org>
   gst-plugins0.8

Sam Hocevar (Debian packages) <sam+deb@zoy.org>
   allegro4.1

eric pareja <xenos@upm.edu.ph>
   kxstitch


Packages being considered for removal from testing to allow the C++ ABI
transition to complete, grouped by maintainer

J.H.M. Dassen (Ray) <jdassen@debian.org>
   pstoedit

Enrique Robledo Arnuncio <era@debian.org>
   rosegarden4

Daniel Baumann <daniel.baumann@panthera-systems.net>
   libextractor

Ben Burton <bab@debian.org>
   kdeaddons

Yann Dirson <dirson@debian.org>
   tulip

Peter Eisentraut <petere@debian.org>
   xmms-kde

Khalid El Fathi <invent@edena-fr.org>
   mypasswordsafe

Peter Hawkins <peterh@debian.org>
   libqt-perl

Mark Hymers <mark.hymers@ncl.ac.uk>
   kst

Teemu Ikonen <tpikonen@pcu.helsinki.fi>
   imview

Aurelien Jarno <aurel32@debian.org>
   keybled

Theodore Karkoulis <bilbo@debian.org>
   kxdocker
   kxdocker-data

Jean-Michel Kelbert <kelbert@debian.org>
   superkaramba

Roman Kreisel <roman.kreisel@web.de>
   krename

Kari Pahula <kari@sammakko.yok.utu.fi>
   droidbattles

Javier Fernandez-Sanguino Pen~a <jfs@computer.org>
   wordtrans

Alejandro Exojo Piqueras <suy@badopi.org>
   kxmleditor

Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
   mapserver

Nick Rusnov <nickrusnov@debian.org>
   ale

Adeodato Simó <asp16@alu.ua.es>
   amarok

Sam Hocevar (Debian packages) <sam+deb@zoy.org>
   langdrill
